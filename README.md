# Minetest Not So Simple Game

The most increible vaste enemy provided game to play and hard to defeats

* English information: [GAMEINFO-en.md](GAMEINFO-en.md), **WARNING: if you question why there is no mod "x" instead of mod "y" read the information file in the initial section **
* Informacion español: [GAMEINFO-es.md](GAMEINFO-es.md), **ADVERTENCIA: si se cuestiona porque no esta el mod "x" en vez de el mod "y" lease el archivo de informacion en la seccion inicial**
* Информация русском: [GAMEINFO-ru.md](GAMEINFO-ru.md), **ВНИМАНИЕ: если у вас возник вопрос, почему нет мода "x" вместо мода "y" прочитайте информационный файл в начальном разделе**

![screenshot.jpg](screenshot.jpg)

For further information, check  https://codeberg.org/minenux/minetest-game-nssg.git 
or https://gitlab.com/minenux/minetest-game-nssg or https://git.minetest.io/minenux/minetest-game-nssg.git

## Installation

For specific minetest installation check [docs/INSTALL.md](docs/INSTALL.md)

- Unzip the archive, rename the folder to `nssg` and
place it in .. `minetest/games/`

- GNU/Linux: If you use a system-wide installation place
it in `~/.minetest/games/` or `/usr/share/games/minetest`.

## Compatibility

The Minetest nssg Game GitHub master HEAD is compatible with MT 0.4.16, 4.0, 5.2 and 5.4
that can be found at https://minetest.io

To download you can play this game with the following minetest engines:

* https://codeberg.org/minenux/minetest-engine-minetest/src/branch/stable-4.1
* https://codeberg.org/minenux/minetest-engine-minetest/src/branch/stable-4.0
* https://codeberg.org/minenux/minetest-engine-minetest/src/branch/stable-5.X

#### Mods

* minetest default and extras
    * integrated the killme/game_commands were simplified into default mod, and provide CC-BY-SA-NC license
    * farming is default older but has build in toolranks support
    * xdecor as `xdecor` [mods/xdecor](mods/xdecor) a super reduced version of homedecor pack, for performance
* sorceredkid auth mod
    * minetest Auth Redux as `auth_rx` [mods/auth_rx](mods/auth_rx) from https://codeberg.org/minenux/minetest-mod-auth_rx
    * so then minetest Formspecs as `formspecs` [mods/formspecs](mods/formspecs) from https://codeberg.org/minenux/minetest-mod-formspecs
* minetest floatlands as `floatlands` [mods/floatlands](mods/floatlands) derived from Floatlands Realm made to this servers, as https://codeberg.org/minenux/minetest-mod-floatland
    * so then default flowers as `flowers` are need. Later this will need ethereal modifications.
* tenplus1 customized mods
    * simple_skins as `skins` [mods/skins](mods/skins) from https://codeberg.org/minenux/minetest-mod-simple_skins
    * regrow as `regrow` [mods/regrow](mods/regrow) from https://codeberg.org/minenux/minetest-mod-regrow
    * ethereal as `ethereal` [mods/ethereal](mods/ethereal) from https://codeberg.org/minenux/minetest-mod-ethereal
    * toolranks as `toolranks` [mods/toolranks](mods/toolranks) from https://codeberg.org/minenux/minetest-mod-toolranks
    * sfinv_home as `sfinv` we already integrated the `sfinv_home` mod into the default `sfinv` mod
* NPXcoot
    * nssf as `nssf` [mods/nssf](mods/nssf) from https://git.minetest.io/minenux/minetest-mod-nssf
    * nsspf as `nsspf` [mods/nsspf](mods/nsspf) from https://git.minetest.io/minenux/minetest-mod-nsspf
* armors and stuff mods
    * 3d_armor & shields [mods/3d_armor](mods/3d_armor) https://codeberg.org/minenux/minetest-mod-3d_armor
    * 3d_armor_globes [mods/3d_armor_gloves](mods/3d_armor_gloves) https://codeberg.org/minenux/minetest-mod-3d_armor_gloves
    * 3d_armor_technic [mods/3d_armor_techincs](mods/3d_armor_technic) https://codeberg.org/minenux/minetest-mod-3d_armor/src/branch/stable-5.0/3d_armor_technic
    * 3d_armor_mobile [mods/3d_armor_mobile](mods/3d_armor_mobile) https://codeberg.org/minenux/minetest-mod-3d_armor/src/branch/stable-4.0/3d_armor_mobile
* player stuffs:
    * minenux bags as `backpacks` [mods/backpacks](mods/backpacks) 
* Shara RedCat ezhh
    * fireflies as `fireflies` [mods/fireflies](mods/fireflies) https://github.com/Ezhh/fireflies/blob/master/license.txt
* Skandarella
    * Wilhelmines Marinara as `marinara` [mods/marinara](mods/marinara) https://git.minetest.io/minenux/minetest-mod-marinara forked compatible version
* Wuzzy
    * Hudbars as `hudbars` [mods/hudbars](mods/hudbars) https://codeberg.org/minenux/minetest-mod-hudbars
    * Hudbar Armor as `hudbars` [mods/hudbars](mods/hudbars) https://codeberg.org/minenux/minetest-game-nssg/commit/39c56ecd26ef559f063bf9effa0b1c58cc6bf969
    * Stamina&Hunger as `hudbars` [mods/hbhunger](mods/hbhunger) https://codeberg.org/minenux/minetest-game-nssg/commit/39c56ecd26ef559f063bf9effa0b1c58cc6bf969

## Licensing

See `LICENSE.txt`
