--------------------------------------------------------
-- Minetest :: Auth Redux Mod v2.10 (auth_rx)
--
-- See README.txt for licensing and release notes.
-- Copyright (c) 2017-2018, Leslie E. Krause
--------------------------------------------------------

-----------------------------------------------------
-- Global Helper Functions
-----------------------------------------------------
local is_46 = minetest.has_feature("add_entity_with_staticdata")

function get_minetest_config( key )
	if is_46 then
		return minetest.settings:get( key )
	else
		return core.setting_get( key )    -- backwards compatibility
	end
end

function convert_ipv4( str )
	if not str then str = "127.0.0.1" end -- started in local game or load the mod incorrectly
	local ref = string.split( str, ".", false )
	return tonumber( ref[ 1 ] ) * 16777216 + tonumber( ref[ 2 ] ) * 65536 + tonumber( ref[ 3 ] ) * 256 + tonumber( ref[ 4 ] )
end

function unpack_address( addr )
        return { math.floor( addr / 16777216 ), math.floor( ( addr % 16777216 ) / 65536 ), math.floor( ( addr % 65536 ) / 256 ), addr % 256 }
end

function get_default_privs( )
	local default_privs = { }
	local config_privs = get_minetest_config( "default_privs" ) or "interact, shout, fast, home"
	for _, p in pairs( string.split( config_privs, "," ) ) do
		table.insert( default_privs, string.trim( p ) )
	end
	return default_privs
end

function unpack_privileges( assigned_privs )
	local privileges = { }
	for _, p in ipairs( assigned_privs ) do
		privileges[ p ] = true
	end
	return privileges
end

function pack_privileges( privileges )
	local assigned_privs = { }
	for p, _ in pairs( privileges ) do
		table.insert( assigned_privs, p )
	end
	return assigned_privs
end
