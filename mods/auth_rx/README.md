minetest mod Auth Redux
=======================

AUTH drop-in faster replacement for authentication handler in minetest.

Information
-----------

Auth Redux is a drop-in replacement for the builtin authentication handler 
of Minetest. It is designed from the ground up to be robust and secure enough 
for use on high-traffic Minetest servers, while also addressing a number of 
outstanding engine bugs 

Its **the most faster equilibrated authentication handler in minetest.** (its like 
if you were using Redist or LevelDB but with very high stability).

![](screenshot.png)

## Technical information
------------------------

Auth Redux provides an efficient and scalable back-end solution for player 
authentication, making it suitable for even the most demanding server 
environments. It has been put to the test with a database of over 520,000 registered 
users -- with no in-game overhead!

Recommended Enhancement: The DataMiner Mod extends the capabilities of Auth Redux, 
providing comprehensive player and server statistics within a graphical user-interface, 
in addition to an API for custom-tailored log analysis and reporting directly 
from the command line.

- [Technical information](#technical-information)
    - [Feature matrix metrics](#feature-matrix-metrics)
    - [Full lua based code](#full-lua-based-code)
    - [increased security and bug free](#increased-security-and-bug-free)
    - [Dependencies](#dependencies)
    - [Documentation](#documentation)
    - [Technical features](#technical-features)
    - [Integrated ruleset debugger](#integrated-ruleset-debugger)
    - [benchmarks](#benchmarks)
- [Changelog](#changelog)
- [Copyright and LICENSE](#copyright-and-license)


#### Feature matrix metrics

Auth Redux is the most equilibrated faster authentication handler:

| Player Auth handler | re-joins     | new join | set privilegies | database size | stability  |
| ------------------- | ------------ | ----------- | ------------ | ----------- | ------------ |
| 5.0+ Builtin        | 4.5 x Faster | 4 x Faster  | 4.2 x Faster | 17% smaller | 100 x stable |
| 0.4.X Builtin       | 592 x Faster | 3 x Faster  | 633 x Faster | 32% smaller | 1000 x stable |
| sauth mod (sqlite)  | 40 x Faster  | 33x Faster  | 48  x Faster | 32% smaller | 4 x stable   |
| SQLite Builtin      | 30 x Faster  | 23x Faster  | 40  x Faster | 30% smaller | 4 x stable   |
| Redist Builtin      | 8 x slower   | 2 x slower  | 1 x slower   | unknow      | 10 x stable  |
| PostgreSQL Builtin  | 35 x Faster  | 33x Faster  | 40  x Faster | 30% smaller | 4 x stable   |

#### Full lua based code

Auth Redux is completely Lua-driven, with no external dependencies. It 
interfaces seamlessly with Minetest's authentication handler API, therefore 
no complicated setup or configuration is required. It is intended to be 
compatible with all versions of Minetest 0.4.14+, providing server administrators 
with both flexible and fine-grained access control.

#### increased security and bug free

It addressing following long time bugs without complicated dependencies:

* #5334 https://github.com/minetest/minetest/issues/5334
* #6783 https://github.com/minetest/minetest/issues/6783
* #4451 https://github.com/minetest/minetest/issues/4451

#### Dependencies:

* default
* formspecs (ActiveFormspecs Mod)

#### Documentation

Original documentation is on https://github.com/sorcerykid/auth_rx/wiki/Introduction 

#### Technical features

* High-Performance Journaling System

To protect the master database against corruption and data loss, all 
transactions are serially recorded within a journal, thereby reducing the 
frequency and intensity of disk writes. In the event of a server crash or 
power outage, any pending transactions are committed to the master database.

* Stateful Rule-Based Login Filtering

MARS is an easy-to-learn scripting language that affords a first line of 
defense against unwanted visitors. Both restrictive and permissive security 
models can be enforced via rulesets, allowing every login request to be 
filtered according to conditional action-chains with custom error messages.

* Expanded Master Database Schema

The master database supports additional metadata to aid in reviewing and monitoring 
login activity, including the timestamp of the first login, the total time 
spent playing, the total number of player sessions, etc. This information 
can prove invaluable for command-line reporting and analysis tools.

#### Integrated ruleset debugger

By entering the `/fdebug` command into chat (requires the "server privilege"), 
you will be presented with a debugging console. The workspace consists of the 
following elements:

![](screenshot-debugger.png)

* A. The "Show Client Output" option toggles whether to display the client output 
panel. The "Show Debug Prompt" option toggles whether to insert debug status prompts 
into the source code.
* B. This textarea contains the ruleset definition to be examined. Although Minetest 
supports editing of text, it is strongly recommended to copy and paste your source 
code into a full-fledged text editor.
* C. The client output panel renders error messages as they would appear within the 
client. The status panel typically indicates whether the ruleset passed or failed, 
as well as other debugging conditions.
* D. The "Save" button will export the current ruleset definition, overwriting 
"greenlist.mt" in your world directory. The "Load" button will import an existing 
ruleset definition from "greenlist.mt" for debugging.
* E. The "Process" button will process the ruleset definition according to the 
selected login filtering criteria: Normal, New Account, or Wrong Password (thereby 
changing the relevant preset variables).
* F. The preset variables are listed here with their corresponding values. These 
values will never change except during the login filtering process, or unless 
explicitly set in the panel below.
* G. The name and type of the selected variable is indicated here. The value can be 
edited in the text field, and set with the "Set" button. The arrow buttons allow for 
re-ordering any variable within the list.

Some variables, like $clock and $uptime, have an "Auto Update" option to toggle whether 
the values should be derived from the system state. For a fully sandboxed environment, 
you can disable this option.

The special `$__debug` variable gives you direct access to the MARS expression 
interpreter. You can enter any valid expression, and the resulting value and type 
will be displayed in the panel above (all variable types, except patterns, are 
supported). This is particularly helpful for monitoring the values of certain 
variables. To calculate the size of the $ip_names_list, for example, you would enter

```
size($ip_names_list)
```

Whenever a ruleset passes or fails, or if a syntax error is encountered, a debug 
status prompt will be inserted into the source code below the line in question:

```
if $name->len() eq "administrator"`
# ====== ^ Line 12: Mismatched operands in ruleset^ ======
```

These breakpoints will be removed automatically when the ruleset definition is saved, 
so there is no need to edit them out.

#### benchmarks

You can check the file [benchmark.txt](benchmark.txt) from where the 
comparisons were made.

As these benchmarks reveal, there is no discernible benefit to using an SQLite3 
backend for authentication over a flat-file database, whether in terms of 
reliability or performance.

The cons of using LEvelDB or REdist is the unstability of those fast storage 
authentication handlers, so auth_RX solves the problem and being more faster.

If anything, SQLite3/File/LevelDB databases are a hindrance when it comes to 
random access of large volumes of data, and more than likely the culprit for 
why some high-traffic servers lately are experiencing notable lag spikes

## Changelog

Please see [changelog](changelog) file

## Copyright and LICENSE

Copyright (c) 2016-2018, Leslie Krause (leslie@searstower.org)
Copyright (c) 2020-2023, PICCORO Lenz McKAY (mckaygerhard@gmail.com)

**License of modified or added source code**

The auto initializer code and improvements were adde under CC BY-SA NC 4.0
by mckaygerhard or "PICCORO" Lenz McKAY (mckaygerhard@gmail.com)

**License of original source files until 2018**

Auth Redux Mod v2.13 By Leslie Krause under The MIT License (MIT)

https://forum.minetest.net/viewtopic.php?f=9&t=20393

Browse orignal source code:  https://bitbucket.org/sorcerykid/auth_rx

Check the [LICENSE](LICENSE) file.

