minetest mod ethereal NG
======================

BIOME mod mapgen to add many new biomes to the world

## Information
--------------

This mod is named `ethereal`, it produces huge amount of new biomes
overriding defaults, many new items and features that spice up gameplay.

Known as Ethereal NG (next-generation) it give players that
little something extra to look for while exploring the world.

![screenshot.png](screenshot.png)

Forum Topic https://forum.minetest.net/viewtopic.php?f=11&t=14638

TODO: document items and crafting

#### Lucky Blocks

It provides 63 new lucky blocks if the mod is detected.

## Technical info
-----------------

- [Information](#information)
    - [Lucky Blocks](#lucky-blocks)
- [Technical info](#technical-info)
    - [Mod structure and customization](#mod-structure-and-customization)
    - [Dependencies](#dependencies-and-compatibility)
    - [Compatibility](#dependencies-and-compatibility)
    - [Settings](#settings)
    - [Fishing api](#fishing-api)
    - [Biomes list](#biomes-list)
    - [Biome work](#biome-work)
  - [Node list](#node-list)
    - [Hard nodes or blocks](#hard-nodes-or-blocks)
    - [Biome nodes like trees and plants](#biome-nodes-like-trees-and-plants)
      - [Willow](#willow)
      - [Redwood](#redwood)
      - [Frost megatree](#frost-megatree)
      - [Banana ](#banana-)
      - [Orange minitree](#orange-minitree)
      - [Healing megatree](#healing-megatree)
      - [Palm tree](#palm-tree)
      - [Birch minitree ](#birch-minitree-)
      - [Bamboo](#bamboo)
      - [Sakura megatree](#sakura-megatree)
      - [Lemon tree](#lemon-tree)
      - [Olive megatree](#olive-megatree)
    - [Other "Wood-like" items](#other-"wood-like"-items)
      - [Mushroom](#mushroom)
      - [Misc bushes](#misc-bushes)
      - [BIG SAPLING](#big-sapling)
   - [Understanding Biome customization](#understanding-biome-customization)
- [Changelog](#changelog)
    - [1.30](#1.30)
    - [1.29](#1.29)
    - [1.28](#1.28)
    - [1.27](#1.27)
    - [1.26](#1.26)
    - [1.25](#1.25)
    - [1.24](#1.24)
    - [1.23](#1.23)
    - [1.22](#1.22)
    - [1.21](#1.21)
    - [1.20](#1.20)
    - [1.19](#1.19)
    - [1.18](#1.18)
    - [1.17](#1.17)
    - [1.16](#1.16)
    - [1.15](#1.15)
    - [1.14](#1.14)
    - [1.13](#1.13)
    - [1.12](#1.12)
    - [1.11](#1.11)
    - [1.10](#1.10)
    - [1.09](#1.09)
- [LICENSE](#license)

#### Mod structure and customization

Biomes customization values are hardcoded in the [`biomes.lua`](biomes.lua) file, 
and initialized in the [`biomes_init.lua`](biomes_init.lua) file.

Nodes and elements are hardcoded in the [schems.lua](schems.lua) file, 
each of those are defined in [schematics](schematics) directory.

Underwater life are defined in the [sealife.lua](sealife.lua) file.

#### Dependencies and compatibility

Make sure you are using Minetest 0.4.16+ and start a new world (anything but v6), 
then enable Ethereal in the mods list. Ethereal has been designed to work 
alongside with those mods:

* default
* farming (Farming Redo, not normal build in, support for seeds, plant and new foods)
* flowers ( for sunflowers, fire flowers and each variety of viola and others)
* mobs (Mobs Redo, is heaviliy used)
* backedclay (For clay biomes and results)
* stairs

Also those more as optional:

* lucky_block (will provide much more surprices for, 60+ surprices)
* doors (will provide a fence gate and a sakura door)
* intllib (only need on older engines for translations)
* toolranks (Will provide support for new crystal stuffs)

#### Settings

**Note:** if your Ethereal world does have a few forest fires appearing 
you can always add this line to your minetest.conf file: `disable_fire = true`


| name setting                   |  Description                     | values | default |
| ------------------------------ | --------------------------------- | ---- | ------ |
| ethereal.leaftype              | 2D plantlike leaves (0) or 3D (1) | int  | 0 |
| ethereal.leafwalk              | Walkable leaves                   | bool | false |
| ethereal.cavedirt              | Caves cut through dirt            | bool | true |
| ethereal.torchdrop             | Torches drop when in water        | bool | true |
| ethereal.papyruswalk           | Papyrus and Bamboo are walkable   | bool | true |
| ethereal.lilywalk              | Lilypads are walkable             | bool | true |
| ethereal.xcraft                | Enable X-Craft cheats             | bool | true |
| ethereal.flight                | Enable Flight Potion              | bool | true |
| ethereal.glacier               | Glacier biome, 1=on / 0=off       | int  | 1 |
| ethereal.bambo                 | Bamboo biome, 1=on / 0=off        | int  | 1 |
| ethereal.mesa                  | Mesa biome, 1=on / 0=off          | int  | 1 |
| ethereal.alpine                | Alpine biome, 1=on / 0=off        | int  | 1 |
| ethereal.healing               | Healing Tree biome, 1=on / 0=off  | int  | 1 |
| ethereal.snowy                 | Pine Tree biome, 1=on / 0=off     | int  | 1 |
| ethereal.frost                 | Blue Frost biome, 1=on / 0=off    | int  | 1 |
| ethereal.grassy                | Grassy biome, 1=on / 0=off        | int  | 1 |
| ethereal.caves                 | Desertstone biome, 1=on / 0=off   | int  | 1 |
| ethereal.grayness              | Grey Willow biome, 1=on / 0=off   | int  | 1 |
| ethereal.grassytwo             | GrassyTwo biome, 1=on / 0=off     | int  | 1 |
| ethereal.prairie               | Prairie biome, 1=on / 0=off       | int  | 1 |
| ethereal.jumble                | Jumble biome, 1=on / 0=off        | int  | 1 |
| ethereal.junglee               | Jungle biome, 1=on / 0=off        | int  | 1 |
| ethereal.desert                | Desert biome, 1=on / 0=off        | int  | 1 |
| ethereal.grove                 | Banana Grove biome, 1=on / 0=off  | int  | 1 |
| ethereal.mushroom              | Mushroom biome, 1=on / 0=off      | int  | 1 |
| ethereal.sandstone             | Sandstone biome, 1=on / 0=off     | int  | 1 |
| ethereal.quicksand             | Quicksand Bank biome, 1=on / 0=off  | int | 1 |
| ethereal.plains                | Scorched Plains biome, 1=on / 0=off | int | 1 |
| ethereal.savanna               | Savanna biome, 1=on / 0=off       | int  | 1 |
| ethereal.fiery                 | Feiry Lava biome, 1=on / 0=off    | int  | 1 |
| ethereal.sandclay              | Sandy Clay biome, 1=on / 0=off    | int  | 1 |
| ethereal.swamp                 | Swamp biome, 1=on / 0=off         | int  | 1 |
| ethereal.sealife               | Coral and Seaweed biome, 1=on / 0=off | int | 1 |
| ethereal.reefs                 | MT5 Coral biome, 1=on / 0=off     | int  | 1 |
| ethereal.sakura                | Sakura biome, 1=on / 0=off        | int  | 1 |
| ethereal.tundra                | Tundra biome, 1=on / 0=off        | int  | 1 |
| ethereal.mediterranean         | Mediterranean biome, 1=on / 0=off | int  | 1 |
| ethereal.flightpotion_duration | Flight Potion Duration seconds    | int  | 300 |

#### Fishing api

The mod provides a fishing api, basically is `ethereal.add_item(fish, junk, bonus)`,
several examples are at the documentation of [api.txt](api.txt) file.

#### Biomes list

There are almost 20 biomes or more, those are indicators of (WIP list.. maybe more):

| biome             | y.min |  y.max |  heat | humidity | enabled/setting |
| ----------------- | ---- | ---- | ----- | --------- | ------------- |
| mountain          |  140 | 31000 | 50 | 50 | 1 (enabled) |
| grassland         |  3 | 71 | 45 | 65 | 1 (enalbed) |
| underground       |  -31000 | -192 | 50 | 50 | 1 (enabled) |
| desert            |  3 | 23 | 35 | 20 | ethereal.desert | 
| desert_ocean      |  -192 | 3 | 35 | 20 | ethereal.desert | 
| bamboo            |  25 | 70 | 45 | 75 | ethereal.bamboo | 
| sakura            |  3 | 25 | 45 | 75 | ethereal.sakura | 
| sakura_ocean      |  -192 | 2 | 45 | 75 | ethereal.sakura | 
| mesa              |  1 | 71 | 25 | 28 | ethereal.mesa | 
| mesa_ocean        |  -192 | 1 | 25 | 28 | ethereal.mesa | 
| coniferous_forest |  4 | 40 | 10 | 40 | ethereal.snowy | 
| taiga             |  40 | 140 | 10 | 40 | ethereal.alpine | 
| frost_floatland   |  1025 | 1750 | 10 | 40 | ethereal.frost |
| frost             |  1 | 71 | 10 | 40 | ethereal.frost |
| frost_ocean       |  -192 | 1 | 10 | 40 | ethereal.frost |
| deciduous_forest  |  3 | 91 | 13 | 40 | ethereal.grassy |
| deciduous_forest_ocean   |  -31000 | 3 | 13 | 40 | ethereal.grassy |
| caves             |  4 | 41 | 15 | 25 | ethereal.caves |
| grayness          |  2 | 41 | 15 | 30 | ethereal.grayness |
| grayness_ocean    |  -28 | 1 | 15 | 30 | ethereal.grayness |
| grassytwo         |  1 | 91 | 15 | 40 | ethereal.grassytwo |
| grassytwo_ocean   |  -192 | 1 | 15 | 40 | ethereal.grassytwo |
| prairie           |  3 | 26 | 20 | 40 | ethereal.prairie |
| prairie_ocean     |  -192 | 1 | 20 | 40 | ethereal.prairie |
| jumble            |  1 | 71 | 25 | 50 | ethereal.jumble |
| jumble_ocean      |  -192 | 1 | 25 | 50 | ethereal.jumble |
| junglee           |  1 | 71 | 30 | 60 | ethereal.junglee |
| junglee_ocean     |  -192 | 1 | 30 | 60 | ethereal.junglee |
| grove             |  3 | 23 | 45 | 35 | ethereal.grove |
| grove_ocean       |  -192 | 2 | 45 | 35 | ethereal.grove |
| mediterranean     |  3 | 50 | 20 | 45 | ethereal.mediterranean |
| mushroom          |  3 | 50 | 45 | 55 | ethereal.mushroom |
| mushroom_ocean    |  -192 | 2 | 45 | 55 | ethereal.mushroom |
| sandstone         |  3 | 23 | 50 | 20 | ethereal.sandstone |
| sandstone_ocean   |  -192 | 2 | 50 | 20 | ethereal.sandstone |
| quicksand         |  1 | 1 | 50 | 38 | ethereal.quicksand |
| plains            |  3 | 25 | 65 | 25 | ethereal.plains |
| plains_ocean      |  -192 | 2 | 55 | 25 | ethereal.plains |
| savanna           |  3 | 50 | 55 | 25 | ethereal.savanna |
| savanna_ocean     |  -192 | 1 | 55 | 25 | ethereal.savanna |
| fiery             |  5 | 20 | 75 | 10 | ethereal.fiery |
| fiery_ocean       |  -192 | 4 | 75 | 10 | ethereal.fiery |
| sandclay          |  1 | 11 | 65 | 2 | ethereal.sandclay |
| swamp             |  1 | 7 | 80 | 90 | ethereal.swamp |
| swamp_ocean       |  -192 | 1 | 80 | 90 | ethereal.swamp |

#### Biome work

This is done by assigning a heat and a humidity value to each biome.
Each biome is assigned with a heat and humidity value between 0 and 100. 

The `ethereal` mod will sustitute all the current biomes and will 
places new ones.. the code do `minetest.clear_registered_biomes()` and 
the `minetest.clear_registered_decorations()` rutines, ores still are 
not well developed and are just defaults and featured.

### Node list

#### Hard nodes or blocks

    * ethereal:etherium_ore, ethereal:stone_with_etherium_ore. Regular ore, cosmetic use like gold ore. 
    * ethereal:crystal_spike, mix 2 Crystal and 2 Mese for a crystal ingot, water bucket, gives ethereal:crystal_ingot, used for silk touch tools, ow OP sword
    * ethereal:crystal_block Block of the above mineral, Only for cosmetic use.
    * ethereal:stone_ladder Ladders made of stone 
    * ethereal:glostone Light block, Same light level as a mese post, decorative

#### Biome nodes like trees and plants

Stairs, Slabs, Outer and Inner variants of wood exists, Just append, _stair _slab _outer_stairs _inner_stair to every wood respectively.

List goes, Leaves, Trunk, Wood, Sapling, and extras

##### Willow

    * ethereal:willow_twig
    * ethereal:willow_trunk
    * ethereal:willow_wood
    * ethereal:willow_sapling

##### Redwood

    * ethereal:redwood_leaves
    * ethereal:redwood_trunk
    * ethereal:redwood_wood
    * ethereal:redwood_sapling - Grows a small redwood tree
    * ethereal:giant_redwood_sapling - The big one, crafted from 2 saplings.

##### Frost megatree

    * ethereal:frost_leaves
    * ethereal:frost_tree - Substantially harder to break
    * ethereal:frost_wood - A bit hard to break
    * ethereal:frost_sapling

##### Banana 

    * ethereal:bananaleaves
    * ethereal:banana_trunk
    * ethereal:banana_wood
    * ethereal:banana_tree_sapling
    * ethereal:banana - Heals 2 hearts

##### Orange minitree

Orange uses apple trunk and wood as base

    * ethereal:orange_leaves
    * ethereal:orange_sapling
    * ethereal:orange - heals 1.5 hearts

##### Healing megatree

    * ethereal:yellowleaves
    * ethereal:yellow_trunk
    * ethereal:yellow_wood
    * ethereal:yellow_tree_sapling
    * ethereal:golden_apple - Heals to full health (It does not heal 10 hearts, just heals to default full)

##### Palm tree

Like pine

    * ethereal:palmleaves
    * ethereal:palm_trunk
    * ethereal:palm_wood
    * ethereal:palmsapling

##### Birch minitree 

Like aspen

    * ethereal:birch_leaves
    * ethereal:birch_trunk
    * ethereal:birch_wood
    * ethereal:birch_sapling

##### Bamboo

    * ethereal:bamboo_trunk - Cheap wood, easy to break a trunk of it
    * ethereal:bamboo_leaves
    * ethereal:bamboo_sprout

##### Sakura megatree

    * ethereal:sakura_leaves
    * ethereal:sakura_leaves2 - Nosense to be honest
    * ethereal:sakura_trunk
    * ethereal:sakura_wood
    * ethereal:sakura_sapling

##### Lemon tree

    * ethereal:lemon_leaves
    * ethereal:lemon_trunk
    * ethereal:lemon_wood
    * ethereal:lemon_sapling
    * ethereal:lemon - TODO: Healing value

##### Olive megatree

    * ethereal:olive_leaves
    * ethereal:olive_trunk
    * ethereal:olive_wood
    * ethereal:olive_sapling

#### Other "Wood-like" items

##### Mushroom

    * ethereal:mushroom_sapling
    * ethereal:mushroom
    * ethereal:mushroom_trunk
    * ethereal:mushroom_pore

##### Misc bushes

    * ethereal:bush3 (wtf?)
    * ethereal:bush2 (Nonsense)
    * ethereal:bush

#### BIG SAPLING

-Crafted with 3 regular apple saplings, gros a big tree with lots of wood and apples

    * ethereal:big_tree_sapling


#### Understanding Biome customization

Are there any settings to enable to correctly use it to customized?
Each biome has a `y.min` and `y.max` values where will be apply.
Also is assigned with a `heat` and `humidity` value between 0 and 100.

As well as drawing them by hand, **you can also create Voronoi diagrams 
using programs such as Geogebra.** Dug up this **ethereal voronoi diagram** 
from Paramat at https://www.geogebra.org/classic/jcc8eyy5 using those basics:

* Firstly GeoGebra is not what we call super user friendly - Quirky 
  would be good descriptor :)
* Depending on your monitor size/aspect ratio you may need to change some 
  of the pane sizes :) large widescreen monitors will be helpfully
* If playing with the above don't try and move the blue dots that'll 
  totally break the height automation
* Update the Height Min/Max Heat/Humidity values in the spreadsheet 
  view, It left open on the right (red box below) - and don't change 
  the formula in Column F, that'll break it.
* When updating click in cell update value then click outside the cell 
  on the top menu bar works well...pressing enter will move you down a 
  cell which is annoying - quirky
* Adjusting to an exact height click on the slider and then update the 
  value in the right most pane "definition" field to an exact value - The 
  above link will open setup like that but as soon as you click on another 
  element it's properties will be displayed (Blue box below)

Legend: heat = x and humidity = y so then by example:

![screenshot-geogebra-ethereal.png](screenshot-geogebra-ethereal.png)

File to import in new geogebra: [ethereal-biomes-defaults-22-Jul-2022-geogebra.ggb](ethereal-biomes-defaults-22-Jul-2022-geogebra.ggb)

For example, the Frost biome has heat=10 and humidity=40 by default

Recommendations:

* https://rubenwardy.com/minetest_modding_book/en/advmap/biomesdeco.html

- [Mod structure and customization](#mod-structure-and-customization)
- [Dependencies](#dependencies-and-compatibility)
- [Compatibility](#dependencies-and-compatibility)
- [Settings](#settings)
- [Fishing api](#fishing-api)
- [Biomes list](#biomes-list)
- [Biome work](#biome-work)

## Changelog

A huge thanks to Chinchow who was the inspiration behind Ethereal and everyone
who helped make this mod bigger and better throughout it's release :)

### 1.30
 - New fish textures by SirroBzeroone and BlueTangs Rock
 - New fish added along with food recipes (thanks BlueTangs Rock)
 - Fishing rod now has 65 uses
 - Fixed willow leaves scaling and forced schematic trunk placement
 - Add 11 new lucky blocks
 - Caverealms' glow bait reduces wait time when fishing
 - Add Basandra Bush and wood items, Add Spore Grass

#### 1.29
 - Use "stratum" to generate mesa biome for stripey goodness
 - Added coloured candles (thanks wRothbard)
 - Rename some biomes to fall inline with default for spawning
 - Make stairs and doors a soft dependency, fix willow recipes (thanks sangeet)
 - Added 'ethereal.flightpotion_duration' setting, potion also works on 0.4.x clients
 - Added olive wood fences, gates, mese posts
 - Added lilac to sakura biome (can be crafted into magenta dye)

#### 1.28

 - Added new Mediterranean biome with Lemon and Olive trees (thanks Felfa)
 - Added Candied Lemon and Olive Oil items and recipe
 - Rewrite stairs and add auto-select function
 - Added Lemonade
 - Added smaller redwood trees, to grow large variety 2 saplings required
 - Added Flight Potion (etherium dust arch, fire dust middle, empty bottle bottom)
 - Added new Fishing mechanics (inspired by Rootyjr's on mineclone2)
 - Added fishing api to add new items (read API.txt file)
 - Certain fish can only be found in specific biomes
 - Rename textures so they begin with ethereal_ (sorry texture pack folks)
 - Override mushroom spread abm to use "group:mushroom"

#### 1.27

 - Added Etherium ore and dust
 - Added sparse decoration of dry grass and shrub to caves biome
 - Added sponges that spawn near coral, dry sponge in furnace to soak up water
 - Added new savanna dirt and decorations
 - Use default grass abms
 - Split and re-write mapgen files for tidier generation
 - Giant Mushroom tops now decay when trunk removed
 - Added Blue Marble to grey biomes and Blue Marble Tile recipe

#### 1.26

 - Added Sakura biome, pink sakura trees and saplings
 - 1 in 10 chance of sakura sapling growing into white sakura
 - Bamboo grows in higher elevation while sakura grows in lower
 - Added sakura wood, stairs, fence, gate and door
 - Added 5.0 checks to add new biomes and decorations
 - Fixed water abm for dry dirt and added check for minetest 5.1 dry dirt also

#### 1.25

 - Converted .mts files into schematic tables for easier editing
 - Added firethorn shrub in glacier areas (can be crafted into jelly)
 - Tweaked mapgen decorations
 - Added more lucky blocks
 - Added igloo to glacier biome
 - 2x2 bamboo = bamboo floor, 3x3 bamboo or 2x bamboo floor = bamboo block, blocks can be made into sticks, bamboo stairs need blocks to craft

#### 1.24

 - Updating code to newer functions, requires Minetest 0.4.16 and above
 - Added food groups to be more compatible with other food mods
 - Bonemeal removed (use Bonemeal mod to replace https://forum.minetest.net/viewtopic.php?f=9&t=16446 )
 - Crystal Ingot recipe requires a bucket of water, unless you are using builtin_item mod where you can mix ingredients by dropping in a pool of water instead

#### 1.23

 - Added bonemeal support for bush sapling and acacia bush sapling
 - Added support for [toolranks] mod if found
 - Reworked Crystal Shovel so it acts more like a normal shovel with soft touch

#### 1.22

 - Added coral and silver sand to mapgen (0.4.15 only)
 - Replaced ethereal:green_dirt with default:dirt_with_grass for mortrees compatibility
 - Mesa biomes are now topped with dirt with dry grass (redwood saplings grow on dry grass)
 - Added bonemeal support for moretree's saplings
 - Added settings.conf file example so that settings remain after mod update
 - Added support for Real Torch so that torches near water drop an unlit torch
 - Added support for new leafdecay functions (0.4.15 dev)
 - Mapgen will use dirt_with_rainforest_litter for jungles if found
 - Added bushes to mapgen

#### 1.21

 - Saplings need clear space above to grow (depending on height of tree)
 - Bonemeal changes to suit new sapling growth
 - Fixes and tweaks
 - Added default Abm overrides
 - Added 4 new plants from baked clay mod to mapgen
 - Added swamp biome to outskirts of bamboo areas

#### 1.20

- Tweaked Ethereal to work with new features and nodes in Minetest 0.4.14
- Added bones found in dirt and bonemeal to make tree's and crops grow quicker
- Tree's follow default rules where saplings need light to grow
- Using default schematics for apple, pine, acacia and jungle tree's
- Tidied and split code into separate files
- Redid coloured grass spread function to run better
- Added support for moreblock's stairsplus feature

#### 1.19

- Added new biome routine to help restructure biomes
- Tweaked biome values so that they are more spread out (no more huge bamboo biome)
- Tweaked biome plant and tree decorations
- Fixed farming compatibility when using hoe on ethereal dirt
- Dirt with dry grass turns into green grass when near water
- Ice or snow above sea level melts into river water
- Disabling ethereal biomes no longer shows error messages
- Fire Flowers re-generate, can also be made into Fire Dust and both are fuel
- Optimize and tidy code

#### 1.18

- Added Birch tree, also stairs; fence and gate
- Added Fire flower to fiery biomes (careful, it hurts)
- Tweaked biomes and decoration slightly
- Added tree_tool for admin to quickly grow tree's
- Cobble to Mossycobble when near water has been toned down a bit

#### 1.17

- Added new Glacier biome filled with snow and ice
- Changed Mesa biome to have more coloured clay patterns
- Changed Bamboo biome to have tall tree-like stalks with leaves that give
- Bamboo sprouts are now saplings to grow new stalks
- Removed farmed mushrooms and replaced with default game mushrooms with spores

#### 1.16

- Added new tree schematics that change when placed with random leaves, fruit and height
- Changed frost dirt so that it no longer freezes water (saves lag)
- Torches cannot be placed next to water, otherwise they drop as items
- Added latest farming redo Bean Bushes to mapgen
- Code tidy (thanks HybridDog) and bug fix
- Banana, Orange and Apple fruits now drop when tree has been removed.

#### 1.15

- Added Staff of Light (thanks Xanthin), crafted from illumishrooms and can turn stone into glostone and back again
- Changed how Crystal Spikes reproduce
- Crystal Ingots now require 2x mese crystal and 2x crystal spikes to craft
- Removed obsidian brick & stairs now they are in default game, also removed pine wood stairs for same reason
- Tidied code and optimized a few routines


#### 1.14

- Changed Ethereal to work with Minetest 0.4.11 update and new mapgen features
- Changed Pine tree's to use default pine wood
- Added pine wood fence, gate and stairs
- Crystal Spikes now re-generate in crystal biomes
- Removed layer mapgen, keeping spread as default


#### 1.13

- Changed melting feature to work with 0.4.11 pre-release now that group:hot and group:melt have been removed


#### 1.12

- Added ability to disable biomes in the init.lua file by setting to 1 (enable) or 0 (disable)
- Supports Framing Redo 1.10 foods


#### 1.11

- Added Stairs for Ethereal wood types, mushroom, dry dirt, obsidian brick and clay
- Added Green Coral which can be used as green dye
- Craft 5x Ice in X pattern to give 5x Snow
- Added Snow and Ice Bricks with their own stairs and slabs which melt when near heat


#### 1.10

- Added Stone Ladders (another use for cobble)
- Craft 5x Gravel in X pattern to give 5 dirt, and 5x dirt in X pattern for 5 sand
- Added Acacia tree's to desert biome (a nice pink wood to build with, thanks to VanessaE for textures)
- Added Acacia fences and gates
- Added Vines, craftable with 2x3 leaves


#### 1.09

- Fixed Quicksand bug where player see's only black when sinking instead of yellow effect, note this will only work on new maps or newly generated areas of map containing quicksand
- Hot nodes will melt ice and snow in a better way
- Few spelling errors sorted for sapling and wood names

## LICENSE

* Code:
    *  The MIT License (MIT) Copyright (c) 2016 TenPlus1
* Art:
    * Free textures from lisheng2121 (shutterstock)
    * Free textures from epicwannehz, Altairas, JMArtsDesign
    * Some from Royalty Free SFX from dreamstime.com
    * Others

Complete details are at the [license.txt](license.txt) file.

