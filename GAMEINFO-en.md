# infinite NSSG game play manifiesto

The NSSG game is the most harder to play

## Information

Take a breath before play, you will need!

## Introduction
---------------

The purpose of this file is not to define exact mechanics or goals, only the main goal, 
the game is a experience of group task, continuous play again others.. but in strategic way.
Players may stole, take and serve stuff from and for others.. like a arachi one world.

Future releases may introduce teams.

#### Premise

Server may or may nor reset the hole world.. ony that players carries will remain.

## Core Gameplay
----------------

The game should encourage the following core gameplay elements:

#### carriers stuffs

Players must take care of their life and have second one backup, cos world can 
be reseted in any moment, only that player carries in their inventory or bag will remain.

#### Settlements

The players should be encouraged to build settlements, and group together to survive.

#### Survival

Survival should take two parts, first should be an ever-present, ever-looming threat
that players fight against. It should be the constant factor that players need to
survive against to progress through the game.

The second is the much smaller threat of basic day-to-day life, and the needs that
come from that. This second threat should never overshadow the first, although
neglecting it could result in a severe disadvantage against it.

#### Exploration

The players should explore and go far away but find ways to move on the hole map faster, 
never will be rewarded for exploring the area around them, although
exploration should be difficult when are more close to spawn zone. The spawn zone 
is pretty dangerous. cos is full and is where players start, and lammers cheaters appears.

## Player Progression
---------------------

The progression is only by itselft.. when gain more important stuff and ways to store, hide 
and revive from their fall and have back up of stuff. Cos other players always willchase resources.

Difficulty should ramp-up over time, danger is increased when more players are joined, cos 
less resources will are available and most potential cheaters come in.

## End Goals

The end goal of the game is to survive, and eventually avoid cheaters 
or normal players beat you, or stolen your stuffs, when you are building and sharing with your friends

